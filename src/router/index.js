import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import Users from '../components/user/users.vue'
import Rights from '../components/power/rights.vue'
import Roles from '../components/power/roles.vue'
import Cates from '../components/goods/cate.vue'
import Params from '../components/goods/params.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    // 访问首页home时重定向到welcome
    { path: '/home', component: Home, redirect: '/welcome', children: [
      // 主体区域welcome子组件
      { path: '/welcome', component: Welcome },
      // 用户列表路由
      { path: '/users', component: Users },
      // 权限列表路由
      { path: '/rights', component: Rights },
      // 角色列表路由
      { path: '/roles', component: Roles },
      // 商品管理路由
      { path: '/categories', component: Cates },
      // 商品分类参数路由
      { path: '/params', component: Params }
    ]}
  ]
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从哪个路径跳转而来
  // next 是一个函数，表示放行
  //     next()  放行    next('/login')  强制跳转

  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
